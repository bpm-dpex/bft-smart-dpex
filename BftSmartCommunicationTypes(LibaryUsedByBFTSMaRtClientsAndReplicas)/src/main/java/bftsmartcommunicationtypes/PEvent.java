package bftsmartcommunicationtypes;

import java.io.Serial;
import java.io.Serializable;

/**
 * Represents an event within a process instance. This class holds details about an activity's
 * execution context, including the stage and variable data involved, user interaction and GIR.
 *
 * This class is used by BFT-SMaRt Replicas and Clients to communicate task action events as serialized bytestreams.
 */
public class PEvent implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    private String activity;
    private String stage;
    private String variables;
    private String user;
    private String globalInstanceReference;
    private String clientId;

    /**
     * Constructs a new PEvent with specified details.
     *
     * @param activity The activity related to the event.
     * @param stage The current stage of the activity.
     * @param variables The variables associated with the event.
     * @param user The user involved with the event.
     * @param globalInstanceReference The GIR.
     * @param clientId Client that added this task action event
     */
    public PEvent(String activity, String stage, String variables, String user, String globalInstanceReference, String clientId) {
        this.activity = activity;
        this.stage = stage;
        this.variables = variables;
        this.user = user;
        this.globalInstanceReference = globalInstanceReference;
        this.clientId = clientId;
    }

    // Getter methods
    public String getActivity() {
        return activity;
    }

    public String getStage() {
        return stage;
    }

    public String getVariables() {
        return variables;
    }

    public String getUser() {
        return user;
    }

    public String getGlobalInstanceReference() {
        return globalInstanceReference;
    }

    public String getClientId() {
        return clientId;
    }


    // Setter methods
    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public void setVariables(String variables) {
        this.variables = variables;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setGlobalInstanceReference(String globalInstanceReference) {
        this.globalInstanceReference = globalInstanceReference;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "PEvent{" +
                "activity='" + activity + '\'' +
                ", stage='" + stage + '\'' +
                ", variables='" + variables + '\'' +
                ", user='" + user + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }
}
