# Ask for the number of nodes
$numberOfNodes = Read-Host -Prompt "Enter the number of nodes"


# START Generate Keys for replicas --------------------------------------------------------------------------------
$keysDir = "./config/ecdsakeys"
# Check if the directory exists
if (Test-Path -Path $keysDir) {
    Remove-Item -Path $keysDir -Recurse -Force -Verbose:$false > $null
}
# Create the directory and suppress output
New-Item -ItemType Directory -Path $keysDir -Verbose:$false > $null
# Generate ECDSA keys for each node
for ($id = 0; $id -lt $numberOfNodes; $id++) {
    $privateKeyFile = "$keysDir/privatekey$id"
    $publicKeyFile = "$keysDir/publickey$id"
    # Using smartrun script to generate keys
    ./smartrun bftsmart.tom.util.ECDSAKeyPairGenerator $id "secp256r1"
}
# END Generate Keys for replicas ----------------------------------------------------------------------------------

# START Generate Keys for clients ---------------------------------------------------------------------------------
$numberOfClients = Read-Host -Prompt "Enter the number of clients"
# Generate ECDSA keys for each client, starting ID at 100
for ($id = 100; $id -lt 100 + $numberOfClients; $id++) {
    $privateKeyFile = "$keysDir/privatekey$id"
    $publicKeyFile = "$keysDir/publickey$id"
    # Using smartrun script to generate keys
    ./smartrun bftsmart.tom.util.ECDSAKeyPairGenerator $id "secp256r1"
}
# Now move the keys from the generated output directory to the directory bftsmart expects
$newKeysDir = "./config/keysECDSA"
# Check if the new directory exists and handle accordingly
if (Test-Path -Path $newKeysDir) {
    Remove-Item -Path $newKeysDir -Recurse -Force -Verbose:$false > $null
}
New-Item -ItemType Directory -Path $newKeysDir -Verbose:$false > $null
Move-Item -Path "$keysDir/*" -Destination $newKeysDir -Force
if (Test-Path -Path $keysDir) {
    Remove-Item -Path $keysDir -Recurse -Force -Verbose:$false > $null
}
# END Generate Keys for clients -----------------------------------------------------------------------------------


# START Generate Keys for SSL_TSL ---------------------------------------------------------------------------------
$sslKeysDir = "./config/keysSSL_TLS"
# Check if the TLS directory exists, delete if it does
if (Test-Path -Path $sslKeysDir) {
    Remove-Item -Path $sslKeysDir -Recurse -Force -Verbose:$false > $null
}
# Create the TLS keys directory
New-Item -ItemType Directory -Path $sslKeysDir -Verbose:$false > $null

# Generate individual keystore for each replica and export/import public keys
$keystorePass = "MySeCreT_2hMOygBwY"
$aliasPrefix = "bftsmartEC"
$keyAlg = "EC"
$keyGroupName = "secp256r1"
$certDName = "CN=BFT-SMaRT"

for ($id = 0; $id -lt $numberOfNodes; $id++) {
$keyStoreFile = "$sslKeysDir/EC_KeyPair_256_$id.pkcs12"
# Generate Keystore with keypair for current replica
$keytoolCmd = "keytool -genkeypair -keyalg $keyAlg -groupname $keyGroupName -alias $aliasPrefix$id -keypass $keystorePass -keystore $keyStoreFile -storepass $keystorePass -dname $certDName"
    Invoke-Expression $keytoolCmd

    # Export public key certificate for current replica
    $certFile = "$sslKeysDir/cert_$id.cer"
$exportCmd = "keytool -exportcert -alias $aliasPrefix$id -file $certFile -keystore $keyStoreFile -storepass $keystorePass"
    Invoke-Expression $exportCmd
}

# Import each other's public key certificate into each keystore
for ($id = 0; $id -lt $numberOfNodes; $id++) {
    $keyStoreFile = "$sslKeysDir/EC_KeyPair_256_$id.pkcs12"
    for ($importId = 0; $importId -lt $numberOfNodes; $importId++) {
        if ($id -ne $importId) {
            $certFile = "$sslKeysDir/cert_$importId.cer"
$importCmd = "keytool -importcert -alias $aliasPrefix$importId -file $certFile -keystore $keyStoreFile -storepass $keystorePass -noprompt"
            Invoke-Expression $importCmd
        }
    }
}
# Remove temporary certificate files
Remove-Item -Path "$sslKeysDir/*.cer" -Force

# END Generate Keys for SSL_TSL -----------------------------------------------------------------------------------


#Remove old config
if (Test-Path -Path "./config/currentView") {
    Remove-Item -Path "./config/currentView" -Force
}


# Modify system.config file
# Calculate the maximum number of faulty replicas (f) for Byzantine fault tolerance
# using the formula f = (n-1)/3 for Byzantine fault tolerance
# Calculated according to Bft-SmartWiki: https://github.com/bft-smart/library/wiki/How-BFT-SMaRt-works
$faultyReplicas = [Math]::Floor(($numberOfNodes - 1) / 3)

# Generate the initial view string (e.g., "0,1,2,3,4" for 5 nodes)
$initialView = (0..($numberOfNodes-1)) -join ","
$configFilePath = "./config/system.config"
$configLines = Get-Content -Path $configFilePath

for ($i = 0; $i -lt $configLines.Count; $i++) {
    if ($configLines[$i] -match "^system.servers.num") {
        $configLines[$i] = "system.servers.num = $numberOfNodes"
    }
    elseif ($configLines[$i] -match "^system.servers.f") {
        $configLines[$i] = "system.servers.f = $faultyReplicas"
    }
    elseif ($configLines[$i] -match "^system.initial.view") {
        $configLines[$i] = "system.initial.view = $initialView"
    }
}
$configLines | Set-Content -Path $configFilePath

# Generate hosts.config file
$hostsConfigLines = @("# hosts.config generated by PowerShell script")
for ($id = 0; $id -lt $numberOfNodes; $id++) {
    $port1 = 11000 + $id * 10
    $port2 = $port1 + 1
    $serviceName = "tracking-server-$id"
    $hostsConfigLines += "$id $serviceName $port1 $port2"
}
$hostsConfigLines -join "`n" | Set-Content -Path "./config/hosts.config"

# Generate Dockerfile with individual COPY commands
#$dockerFileContent = @"
## Stage 1: Build the project with Gradle
#FROM gradle:7.2.0-jdk17 AS build
#WORKDIR /home/gradle/src
#
#USER gradle
#
## Copy the entire project into the image's working directory
#COPY --chown=gradle:gradle . .
#
#RUN gradle build --no-daemon
#
##  Stage 2
#FROM openjdk:17-alpine
#WORKDIR /app
#
#COPY --from=build /home/gradle/src/build/classes/java/main /app/classes
#COPY --from=build /home/gradle/src/libs /app/libs
#
#COPY --from=build /home/gradle/src/config /app/config
#
#CMD ["java", "-cp", "libs/*:classes", "bftsmartserver.TrackingServer"]
#"@
#Set-Content -Path "./Dockerfile" -Value $dockerFileContent
# Generate Dockerfile with specific SSL key handling

$dockerFileContent = @"
# Stage 1: Build the project with Gradle
FROM gradle:7.2.0-jdk17 AS build
WORKDIR /home/gradle/src

USER gradle

# Copy the entire project into the image's working directory
COPY --chown=gradle:gradle . .

RUN gradle build --no-daemon

# Stage 2
FROM openjdk:17-alpine
WORKDIR /app

ARG SERVER_ID

COPY --from=build /home/gradle/src/build/classes/java/main /app/classes
COPY --from=build /home/gradle/src/libs /app/libs

# Copy the general configuration files
COPY --from=build /home/gradle/src/config /app/config

# Remove the all keystore files for SSL/TLS and only copy the keystore for the current replica for key testing
RUN rm -rf /app/config/keysSSL_TLS
COPY --from=build /home/gradle/src/config/keysSSL_TLS/EC_KeyPair_256_`${SERVER_ID}.pkcs12 /app/config/keysSSL_TLS/EC_KeyPair_256.pkcs12
# Remove all privatekey files except the one for the current replica for key testing
RUN find /app/config/keysECDSA -type f -name "privatekey*" -and -not -name "privatekey`${SERVER_ID}" -delete

CMD ["java", "-cp", "libs/*:classes", "bftsmartserver.TrackingServer"]
"@
Set-Content -Path "./Dockerfile" -Value $dockerFileContent


# Generate docker-compose.yml file with depends_on for each server
$composeContent = @("version: '3.9'", "services:")
for ($id = 0; $id -lt $numberOfNodes; $id++) {
    $port1 = 11000 + $id * 10
    $dependsOnContent = 0..($id-1) | ForEach-Object { "      - tracking-server-${_}`n" }
    $dependsOn = $dependsOnContent -join ""
    $serviceContent = @(
    "  tracking-server-${id}:",
    "    build:",
    "      context: .",
    "      args:",
    "        SERVER_ID: $id",
    "    environment:",
    "      - SERVER_ID=${id}",
    "    ports:",
    "      - '${port1}:${port1}'",
    "    command: java -cp libs/*:classes bftsmartserver.TrackingServer ${id}"
    )
    if ($id -gt 0) {
        $serviceContent += "    depends_on:`n$dependsOn"
    }
    $serviceContent += "    networks:`n      - tracking-network"
    $composeContent += $serviceContent -join "`n"
}
$composeContent += "`nnetworks:`n  tracking-network:"
$composeFileContent = $composeContent -join "`n"
Set-Content -Path "./docker-compose.yml" -Value $composeFileContent


# Read hosts.config and extract IPs and ports
$hostsConfigPath = "./config/hosts.config"
$hostsInfo = Get-Content -Path $hostsConfigPath | Where-Object {$_ -notmatch "^#" -and $_.Trim() -ne ""} | ForEach-Object {
    $parts = $_ -split ' '
    "Server-ID: $($parts[0]) IP: $($parts[1]) Ports: $($parts[2])[For Clients], $($parts[3])[For Server Replicas]"
}
# Output the final message
Write-Host "`nGenerated the docker and configuration files for the BFTSmart Network:"
$hostsInfo | ForEach-Object { Write-Host $_ }

Write-Host "`nConfiguration completed. Run the Network with 'docker-compose up --build'."