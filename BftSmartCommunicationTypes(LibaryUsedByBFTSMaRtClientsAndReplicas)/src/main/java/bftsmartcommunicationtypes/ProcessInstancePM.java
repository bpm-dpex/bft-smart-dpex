package bftsmartcommunicationtypes;

import bftsmartcommunicationtypes.PEvent;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Represents a process instance, storing information about the instance such as name, variables, events, GAR and GIR.
 *
 * This class is used by BFT-SMaRt Replicas and Clients to communicate instantiation events as serialized bytestreams.
 */
public class ProcessInstancePM implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private Map<Integer, PEvent> events;

    private int curr_event_id;

    private String name;
    private String processVariables;

    private String globalAllianceReference;
    private String globalInstanceReference;
    private String clientId;

    /**
     * Constructs a new ProcessInstancePM with specified details.
     *
     * @param name Name of the process instance.
     * @param processVariables Variables associated with the process instance.
     * @param globalAllianceReference The GAR.
     * @param globalInstanceReference The GIR.
     * @param clientId Client that added this instantiation event.
     */
    public ProcessInstancePM(String name, String processVariables, String globalAllianceReference, String globalInstanceReference, String clientId) {
        this.events = new TreeMap<>(); // Create an empty ArrayList for instances
        this.processVariables = processVariables; // Set the processVariables
        this.name = name;
        this.curr_event_id = 0;
        this.globalAllianceReference = globalAllianceReference;
        this.globalInstanceReference = globalInstanceReference;
        this.clientId = clientId;
    }

    public void addEvent(PEvent event) {
        events.put(curr_event_id, event);
        this.curr_event_id++;
    }

    public Map<Integer, PEvent> getAllEvents() {
        return events;
    }

    public int getNumberOfEvents() {
        return events.size();
    }

    public boolean isEventsEmpty() {
        return events.isEmpty();
    }

    public PEvent getEvent(int index) {
        if (events.containsKey(index)) {
            return events.get(index);
        }
        return null;
    }

    public void clearEvents() {
        events.clear();
    }

    public PEvent removeEventAtIndex(int index) {
        if (events.containsKey(index)) {
            return events.remove(index);
        }
        return null;
    }

    public String getName() {
        return name;
    }
    public String getClientId() {
        return clientId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProcessVariables() {
        return processVariables;
    }

    public void setProcessVariables(String processVariables) {
        this.processVariables = processVariables;
    }

    public String getGlobalAllianceReference() {
        return globalAllianceReference;
    }

    public void setGlobalAllianceReference(String globalAllianceReference) {
        this.globalAllianceReference = globalAllianceReference;
    }

    public String getGlobalInstanceReference() {
        return globalInstanceReference;
    }

    public void setGlobalInstanceReference(String globalInstanceReference) {
        this.globalInstanceReference = globalInstanceReference;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ProcessInstancePM {")
                .append("name='").append(name).append('\'')
                .append(", processVariables='").append(processVariables).append('\'')
                .append(", globalAllianceReference='").append(globalAllianceReference).append('\'')
                .append(", globalInstanceReference='").append(globalInstanceReference).append('\'')
                .append(", clientId='").append(clientId).append('\'')
                .append(", events=[");

        if (!events.isEmpty()) {
            events.forEach((eventId, event) ->
                    sb.append("\n\tEvent ").append(eventId).append(": ").append(event.toString()));
        } else {
            sb.append("No events");
        }

        sb.append("\n]}");
        return sb.toString();
    }

}
