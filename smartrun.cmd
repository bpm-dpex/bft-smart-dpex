@echo off
setlocal
set BASE_DIR=%~dp0
java -cp "%BASE_DIR%libs\\*;%BASE_DIR%build\\classes\\java\\main" -Djava.security.properties="%BASE_DIR%config\\java.security" -Dlogback.configurationFile="%BASE_DIR%config\\logback.xml" %*
endlocal