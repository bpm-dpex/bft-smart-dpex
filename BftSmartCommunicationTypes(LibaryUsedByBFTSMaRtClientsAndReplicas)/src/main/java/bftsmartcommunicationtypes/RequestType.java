package bftsmartcommunicationtypes;

/**
 * This enum defines the various operations that the replicas in DPEX can execute.
 * The RequestType needs to be the first element in the bytestreams sent between BFTSMaRt Clients and Replicas.
 */
public enum RequestType {
    /**
     * Request to instantiate a new process instance.
     */
    INSTANTIATE,

    /**
     * Request to add an event to an existing process instance.
     */
    PUT_EVENT,

    /**
     * Request to remove an event from an existing process instance.
     */
    REMOVE_EVENT,

    /**
     * Request to retrieve all process instance IDs currently managed by the server.
     */
    GET_ALL_INSTANCE_IDS,

    /**
     * Request to retrieve all events associated with a specific process instance.
     */
    GET_ALL_EVENTS_FROM_INSTANCE,

    /**
     * Request to check for the existence of any process instances associated with a specified alliance.
     */
    CHECK_FOR_ALLIANCE_EXISTENCE
}
