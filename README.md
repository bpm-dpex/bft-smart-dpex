# BFT SMaRt Network Setup for DPEX

This guide provides instructions on how to set up a BFT SMaRt network for DPEX using Docker. It involves generating necessary configuration files and initializing a network of Docker containers representing the BFT SMaRt nodes.

## Prerequisites

- Docker and Docker Compose installed on your system.
- PowerShell

## Steps to Initialize the Network

### Step 1: Generate Configuration and Docker Files

Run the provided PowerShell script to generate all necessary configuration and Docker files. This script will prompt you to enter the desired number of replicas for your BFT SMaRt network.

1. Open a Terminal.
2. Navigate to the root directory of the DPEX project where the `setup-docker.ps1` script is located.
3. Run the script by executing:

   ```powershell
   powershell.exe -ExecutionPolicy Bypass -File "./setup-docker.ps1"
   ```

4. When prompted, enter the number of replicas you wish to have in your network.

The script will automatically generate the configuration files, key files and Docker Compose files necessary to start your BFT SMaRt network.

### Step 2: Build the Project

Before proceeding with the Docker setup, it's crucial to build the DPEX project at least once. This step ensures that all necessary binaries and dependencies are correctly packaged.

1. In the Terminal, ensure you are still in the project's root directory.
2. Execute the following command to build the project:

   ```shell
   ./gradlew build
   ```

   - Note: Windows users may need to run `gradlew.bat build` if not using a Unix-like shell.

This command compiles the Java source code, runs any tests, and generates the necessary artifacts for the application.

### Step 3: Start the Network Using Docker Compose

After generating the configuration files, use Docker Compose to start the network:

1. In the same Terminal or a command prompt, ensure you are still in the project's root directory.
2. Run the following command to build and start the network:

   ```powershell
   docker-compose up --build
   ```

This command will build the Docker images and start the containers as defined in the Docker Compose file. Each container represents a node in your BFT SMaRt network.

### Step 4: Interacting with the Network

Once the BFT SMaRt Network is operational, you can connect to it through the DPEX web frontend. This setup serves as an alternative to blockchain synchronization methods. To connect, use the IP address of the host system running the BFT SMaRt Network along with the ports provided by the setup script.

### Step 5: Stopping the Network

To stop and remove the network containers, you can use the following Docker Compose command:

```powershell
docker-compose down
```

## Alternative Setup: Single Replica Deployment

In a production environment all replicas will be hosted by different process participants in DPEX, follow these instructions to configure and initialize a single BFT SMaRt replica with which you want to participate in an actually distributed setup.

### Step 1: Prepare Configuration Files

1. Open a PowerShell terminal.
2. Navigate to the root directory of the DPEX project.
3. Execute the setup script for a single instance by running:

   ```powershell
   powershell.exe -ExecutionPolicy Bypass -File "./setup-docker-single-instance.ps1"
   ```

4. Input the required information when prompted:
   - Number of nodes
   - Client ID assigned to the process participant
   - Replica ID assigned to the participant
   - IP addresses and ports for each replica participating in the network

The script will use this information to populate the configuration files for BFT SMaRt and generate a TLS/SSL keystore for your specified replica and ECDSA key pairs for the replica and client ID specified by you.

### Step 2: Import External Certificates

Before building and starting the replica:

1. Share your public keyfiles and certificate with the other process participants
2. Manually move the public key files of other replicas and clients into the `keysECDSA` directory.
3. Place the `.cer` files in the `keysSSL_TLS` directory.
4. Run the following script to import these external certificates into your keystore:

   ```powershell
   powershell.exe -ExecutionPolicy Bypass -File "./import-external-certs.ps1"
   ```


### Step 3: Build and Start the Replica with Docker Compose

1. Ensure you are still in the project's root directory.
2. Execute the following commands to build the project and start the replica:

   ```shell
   docker-compose build
   docker-compose up
   ```

This will initialize your own replica using Docker, incorporating all necessary configurations and security measures.

### Step 4: Interacting with the Network

Only after the network is fully initialized, you can connect to the BFT SMaRt Network through the DPEX web frontend using the IP address and the ports you specified in the setup script.

### Step 5: Stopping the Network

To stop your Docker containers

and clean up resources, execute:

```powershell
docker-compose down
```

## Troubleshooting

If you encounter issues during the setup or operation of your BFT SMaRt network, consider the following troubleshooting steps:

- Verify that Docker and Docker Compose are correctly installed and up-to-date.
- Ensure that PowerShell is correctly set up and can execute scripts (check execution policies if necessary).
- Check the Docker and Docker Compose logs for any error messages that can provide insights into what might be going wrong.
- Make sure you have built the DPEX project using Gradle before attempting to start the Docker network.

For further assistance, consult the [Docker documentation](https://docs.docker.com/) and [PowerShell documentation](https://docs.microsoft.com/en-us/powershell/).