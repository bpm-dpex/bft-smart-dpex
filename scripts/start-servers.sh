#!/bin/bash

# Lesen der hosts.config und Starten der Serverinstanzen
while IFS=' ' read -r id ip port1 port2 _; do
  if [[ $id != \#* ]]; then
    echo "Starting server instance with ID: $id, IP: $ip, Port: $port1"
    java -cp "libs/*:build/classes/java/main" bftsmartserver.TrackingServer $id &
  fi
done < /app/config/hosts.config

# Warten auf das Beenden aller Hintergrundprozesse
wait
