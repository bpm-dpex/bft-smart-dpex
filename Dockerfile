# Stage 1: Build the project with Gradle
FROM gradle:7.2.0-jdk17 AS build
WORKDIR /home/gradle/src

# Make sure directories are owned by gradle before switching user
USER root
RUN chown -R gradle:gradle /home/gradle
USER gradle

# Copy the entire project into the image's working directory
COPY --chown=gradle:gradle . .

RUN gradle build --no-daemon --stacktrace --info

# Stage 2
FROM openjdk:17-alpine
WORKDIR /app

ARG SERVER_ID

COPY --from=build /home/gradle/src/build/classes/java/main /app/classes
COPY --from=build /home/gradle/src/libs /app/libs

# Copy the general configuration files
COPY --from=build /home/gradle/src/config /app/config

# Remove the all keystore files for SSL/TLS and only copy the keystore for the current replica for key testing
RUN rm -rf /app/config/keysSSL_TLS
COPY --from=build /home/gradle/src/config/keysSSL_TLS/EC_KeyPair_256_${SERVER_ID}.pkcs12 /app/config/keysSSL_TLS/EC_KeyPair_256.pkcs12
# Remove all privatekey files except the one for the current replica for key testing
RUN find /app/config/keysECDSA -type f -name "privatekey*" -and -not -name "privatekey${SERVER_ID}" -delete

CMD ["java", "-cp", "libs/*:classes", "bftsmartserver.TrackingServer"]