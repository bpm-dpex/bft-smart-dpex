
$sslKeysDir = "./config/keysSSL_TLS"
$keyStoreFile = "$sslKeysDir/EC_KeyPair_256.pkcs12"
$keystorePass = "MySeCreT_2hMOygBwY"

$certFiles = Get-ChildItem -Path $sslKeysDir -Filter "*.cer"
if (-Not (Test-Path -Path $keyStoreFile)) {
    Write-Host "Keystore file not found: $keyStoreFile"
    exit
}
foreach ($cert in $certFiles) {
    $alias = $cert.BaseName
    $importCmd = "keytool -importcert -noprompt -trustcacerts -alias $alias -file `"$($cert.FullName)`" -keystore `"$keyStoreFile`" -storepass $keystorePass"
    try {
        Invoke-Expression $importCmd
        Write-Host "Successfully imported: $($cert.Name)"
    } catch {
        Write-Host "Failed to import: $($cert.Name). Error: $_"
    }
}

Write-Host "Certificate import process completed."
