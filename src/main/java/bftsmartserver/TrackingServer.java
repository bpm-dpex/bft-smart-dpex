package bftsmartserver;

import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.server.defaultservices.DefaultSingleRecoverable;

import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import bftsmartcommunicationtypes.PEvent;
import bftsmartcommunicationtypes.RequestType;
import bftsmartcommunicationtypes.ProcessInstancePM;

/**
 * The TrackingServer class represents a replica for the DPEX Framework's BFT-SMaRt SCI Connector
 */
public class TrackingServer extends DefaultSingleRecoverable {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private Map<String, ProcessInstancePM> processInstancesMap;
    private Logger logger;
    private int latestFreeInstanceId = 0;

    public TrackingServer(int id) {
        processInstancesMap = new TreeMap<>();
        logger = Logger.getLogger(TrackingServer.class.getName());
        new ServiceReplica(id, this, this);
    }

    /**
     * The main method to start the replica.
     *
     * @param args Command line arguments, expects the replica id as the first argument.
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: bftsmartserver.TrackingServer <server id>");
            System.exit(-1);
        }
        new TrackingServer(Integer.parseInt(args[0]));
    }

    /**
     * Executes ordered operations on the replica, e.g. receiving a new task action event.
     *
     * @param command The command to be executed.
     * @param msgCtx The message context.
     * @return A byte array containing the serialized response to the command.
     */
    @Override
    public byte[] appExecuteOrdered(byte[] command, MessageContext msgCtx) {
        byte[] reply = null;
        boolean hasReply = false;

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(command);
             ObjectInput objIn = new ObjectInputStream(byteIn);
             ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);) {
            RequestType reqType = (RequestType) objIn.readObject();


            switch (reqType) {
                case INSTANTIATE: // Returns ID of new Process Instance
                    String globalAllianceReference = (String) objIn.readObject();
                    String name = (String) objIn.readObject();
                    String variables = (String) objIn.readObject();
                    String globalInstanceReference =  Integer.toString(latestFreeInstanceId);
                    latestFreeInstanceId++;
                    ProcessInstancePM newPi = new ProcessInstancePM(name, variables, globalAllianceReference, globalInstanceReference, Integer.toString(msgCtx.getSender()));
                    ProcessInstancePM retValue = processInstancesMap.put(globalInstanceReference, newPi);
                    if (retValue != null) {
                        throw new RuntimeException("An illegal operation occured, an old process instance was overwritten.");
                    }
                    logger.info("ProcessInstance returned:" + newPi.getGlobalAllianceReference());
                    objOut.writeObject(globalInstanceReference);
                    objOut.writeObject(newPi);
                    hasReply = true;
                    break;
                case PUT_EVENT:
                    String processId = (String) objIn.readObject();
                    PEvent newEvent = (PEvent) objIn.readObject();
                    newEvent.setClientId(Integer.toString(msgCtx.getSender()));
                    if (!processInstancesMap.containsKey(processId)) {
                        throw new RuntimeException("Instance with requested ID could not be found.");
                    }
                    ProcessInstancePM currInstance = processInstancesMap.get(processId);
                    currInstance.addEvent(newEvent);
                    objOut.writeObject(currInstance.getNumberOfEvents() - 1);
                    hasReply = true;
                    break;
                case REMOVE_EVENT:
                    String processId2 = (String) objIn.readObject();
                    int eventId = (int) objIn.readObject();

                    if (!processInstancesMap.containsKey(processId2)) {
                        throw new RuntimeException("Instance with requested ID could not be found.");
                    }

                    PEvent retEvent = processInstancesMap.get(processId2).removeEventAtIndex(eventId);
                    if (retEvent == null) {
                        throw new RuntimeException("Event to delete could not be found.");
                    } else {
                        objOut.writeObject(retEvent);
                        hasReply = true;
                    }
                    break;
                case GET_ALL_INSTANCE_IDS:
                    objOut.writeObject(processInstancesMap.keySet());
                    hasReply = true;
                    break;
                case GET_ALL_EVENTS_FROM_INSTANCE:
                    String processId3 = (String) objIn.readObject();
                    if (!processInstancesMap.containsKey(processId3)) {
                        throw new RuntimeException("Instance with requested ID could not be found.");
                    }
                    objOut.writeObject(processInstancesMap.get(processId3).getAllEvents());
                    hasReply = true;
                    break;
                case CHECK_FOR_ALLIANCE_EXISTENCE:
                    String allianceRef = (String) objIn.readObject();
                    List<ProcessInstancePM> instancesForAlliance = new ArrayList<>();
                    for (ProcessInstancePM instance : processInstancesMap.values()) {
                        if (instance.getGlobalAllianceReference().equals(allianceRef)) {
                            instancesForAlliance.add(instance);
                        }
                    }
                    if (instancesForAlliance.isEmpty()) {
                        logger.info("No instances found for the given alliance reference.");
                    }
                    objOut.writeObject(instancesForAlliance);
                    hasReply = true;
                    break;
            }
            if (hasReply) {
                objOut.flush();
                byteOut.flush();
                reply = byteOut.toByteArray();
            } else {
                reply = new byte[0];
            }

        } catch (IOException | ClassNotFoundException | RuntimeException e) {
            logger.log(Level.SEVERE, "Ocurred during map operation execution", e);
        }

        if (reply == null) {
            logger.log(Level.SEVERE, "Reply was null");
        }
        return reply;
    }

    /**
     * Executes unordered operations on the replica.
     * Must be read-only operations, like e.g. getting all events for a process instance.
     *
     * @param command The command to be executed.
     * @param msgCtx The message context.
     * @return A byte array containing the serialized response to the command.
     */
    @SuppressWarnings("unchecked")
    @Override
    public byte[] appExecuteUnordered(byte[] command, MessageContext msgCtx) {
        byte[] reply = null;
        boolean hasReply = false;

        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(command);
             ObjectInput objIn = new ObjectInputStream(byteIn);
             ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);)
        {

            RequestType reqType = (RequestType) objIn.readObject();
            switch (reqType) {
                case GET_ALL_INSTANCE_IDS:
                    objOut.writeObject(new TreeSet<>(processInstancesMap.keySet()));
                    hasReply = true;
                    break;
                case GET_ALL_EVENTS_FROM_INSTANCE:
                    String processId3 = (String) objIn.readObject();
                    if (!processInstancesMap.containsKey(processId3)) {
                        throw new RuntimeException("Instance with requested ID could not be found.");
                    }
                    objOut.writeObject(processInstancesMap.get(processId3).getAllEvents());
                    hasReply = true;
                    break;
                case CHECK_FOR_ALLIANCE_EXISTENCE:
                    String allianceRef = (String) objIn.readObject();
                    List<ProcessInstancePM> instancesForAlliance = new ArrayList<>();
                    for (ProcessInstancePM instance : processInstancesMap.values()) {
                        if (instance.getGlobalAllianceReference().equals(allianceRef)) {
                            instancesForAlliance.add(instance);
                        }
                    }
                    if (instancesForAlliance.isEmpty()) {
                        logger.info("No instances found for the given alliance reference.");
                    }
                    objOut.writeObject(instancesForAlliance);
                    hasReply = true;
                    break;
                default:
                    logger.log(Level.WARNING, "in appExecuteUnordered only read operations are supported");
            }
            if (hasReply) {
                objOut.flush();
                byteOut.flush();
                reply = byteOut.toByteArray();
            } else {
                reply = new byte[0];
            }
        } catch (IOException | ClassNotFoundException | RuntimeException e) {
            logger.log(Level.SEVERE, "Ocurred during map operation execution", e);
        }

        if (reply == null) {
            logger.log(Level.SEVERE, "Reply was null");
        }

        return reply;
    }

    /**
     * Retrieves the replicas current state for the state transfer protocol
     *
     * @return A byte array containing the serialized state of the replica.
     */
    @Override
    public byte[] getSnapshot() {
        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {
            objOut.writeObject(processInstancesMap);
            objOut.writeObject(latestFreeInstanceId);
            return byteOut.toByteArray();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while getting snapshot", e);
        }
        return new byte[0];
    }

    /**
     * Installs a snapshot of the state onto the replica (used in the state transfer protocol).
     *
     * @param state The byte array containing the serialized state to be installed.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void installSnapshot(byte[] state) {
        try (ByteArrayInputStream byteIn = new ByteArrayInputStream(state);
             ObjectInput objIn = new ObjectInputStream(byteIn)) {
            processInstancesMap = (Map<String, ProcessInstancePM>) objIn.readObject();
            latestFreeInstanceId = (int) objIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            logger.log(Level.SEVERE, "Error while installing snapshot", e);
        }
    }
}